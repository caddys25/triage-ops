# frozen_string_literal: true

RSpec.shared_examples 'rate limited' do |count: Triage::RateLimit::DEFAULT_RATE_LIMIT_COUNT, period: Triage::RateLimit::DEFAULT_RATE_LIMIT_PERIOD|
  it 'does not post a comment if number of commands sent exceeds the allowed limit within the period', :clean_cache do
    start_time = Time.now

    # exhaust rate limit
    expect_comment_request(times: count, event: event, body: anything) do
      count.times { subject.triage }
    end

    # Subsequent events are rate limited
    expect_comment_request(times: 0, event: event, body: anything) do
      subject.triage
    end

    # 1 second before rate limit expiry time
    Timecop.freeze(start_time + period - 1) do
      expect_comment_request(times: 0, event: event, body: anything) do
        subject.triage
      end
    end

    # at rate limit expiry time
    Timecop.freeze(start_time + period) do
      expect_comment_request(times: 0, event: event, body: anything) do
        subject.triage
      end
    end

    # 1 second after rate limit expiry time
    Timecop.freeze(start_time + period + 1) do
      expect_comment_request(times: 1, event: event, body: anything) do
        subject.triage
      end
    end
  end
end

RSpec.shared_examples 'rate limited discussion notes requests' do |count: Triage::RateLimit::DEFAULT_RATE_LIMIT_COUNT, period: Triage::RateLimit::DEFAULT_RATE_LIMIT_PERIOD|
  it 'does not post a discussion if number of commands sent exceeds the allowed limit within the period', :clean_cache do
    start_time = Time.now

    # exhaust rate limit
    expect_discussion_notes_request(times: count, event: event, body: anything) do
      count.times { subject.triage }
    end

    # Subsequent events are rate limited
    expect_discussion_notes_request(times: 0, event: event, body: anything) do
      subject.triage
    end

    # 1 second before rate limit expiry time
    Timecop.freeze(start_time + period - 1) do
      expect_discussion_notes_request(times: 0, event: event, body: anything) do
        subject.triage
      end
    end

    # at rate limit expiry time
    Timecop.freeze(start_time + period) do
      expect_discussion_notes_request(times: 0, event: event, body: anything) do
        subject.triage
      end
    end

    # 1 second after rate limit expiry time
    Timecop.freeze(start_time + period + 1) do
      expect_discussion_notes_request(times: 1, event: event, body: anything) do
        subject.triage
      end
    end
  end
end

RSpec.shared_examples 'rate limited Slack message posting' do |count: Triage::RateLimit::DEFAULT_RATE_LIMIT_COUNT, period: Triage::RateLimit::DEFAULT_RATE_LIMIT_PERIOD|
  it 'does not post a comment if number of commands sent exceeds the allowed limit within the period', :clean_cache do
    start_time = Time.now

    # exhaust rate limit
    count.times { subject.triage }
    expect(messenger_stub).to have_received(:ping).exactly(count).times.with(anything).ordered

    # Subsequent events are rate limited
    subject.triage

    # 1 second before rate limit expiry time
    Timecop.freeze(start_time + period - 1) do
      subject.triage
    end

    # at rate limit expiry time
    Timecop.freeze(start_time + period) do
      subject.triage
    end

    # 1 second after rate limit expiry time
    Timecop.freeze(start_time + period + 1) do
      expect(messenger_stub).to receive(:ping).once.with(anything).ordered

      subject.triage
    end
  end
end
