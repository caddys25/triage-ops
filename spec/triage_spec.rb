# frozen_string_literal: true
require 'spec_helper'

require_relative '../triage/triage'

RSpec.describe Triage do
  shared_examples 'cached group member attributes' do |method_name, attr, group, expires_in|
    let(:method) { ->(**args) { described_class.public_send(method_name, **args) } }
    let(:escaped_group) { CGI.escape(group) }
    let(:members_response_1) { [{ id: 1, username: 'root' }] }
    let(:members_response_2) { [{ id: 2, username: 'another-user' }] }
    let(:members_attrs_1) { members_response_1.map { |h| h[attr] } }
    let(:members_attrs_2) { members_response_2.map { |h| h[attr] } }

    it 'caches the API response', :clean_cache do
      expect_api_request(path: "/groups/#{escaped_group}/members", query: { per_page: 100 }, response_body: members_response_1) do
        # Populate the cache
        expect(method.call).to eq(members_attrs_1)
      end

      # Next call is retrieved from cache
      expect(method.call).to eq(members_attrs_1)

      # Travel 1 second after the cache expires
      Timecop.travel(Time.now + expires_in + 1) do
        expect_api_request(path: "/groups/#{escaped_group}/members", query: { per_page: 100 }, response_body: members_response_2) do
          # Ensure the response is up-to-date
          expect(method.call).to eq(members_attrs_2)
        end

        # Next call is retrieved from cache
        expect(method.call).to eq(members_attrs_2)
      end
    end

    context 'when `fresh: true` is passed', :clean_cache do
      it 'calls the API and saves the response in the cache' do
        expect_api_request(path: "/groups/#{escaped_group}/members", query: { per_page: 100 }, response_body: members_response_1) do
          # Populate the cache
          expect(method.call).to eq(members_attrs_1)
        end

        expect_api_request(path: "/groups/#{escaped_group}/members", query: { per_page: 100 }, response_body: members_response_2) do
          # Ensure the response is up-to-date
          expect(method.call(fresh: true)).to eq(members_attrs_2)
        end

        # Next call without `fresh: true` is retrieved from cache
        expect(method.call).to eq(members_attrs_2)
      end
    end
  end

  describe '.gitlab_org_group_member_ids' do
    it_behaves_like 'cached group member attributes', :gitlab_org_group_member_ids, :id, described_class::GITLAB_ORG_GROUP, described_class::GROUP_CACHE_DEFAULT_EXPIRATION
  end

  describe '.gitlab_org_contractors_member_ids' do
    it_behaves_like 'cached group member attributes', :gitlab_org_contractors_member_ids, :id, described_class::GITLAB_ORG_CONTRACTORS_GROUP, described_class::GROUP_CACHE_DEFAULT_EXPIRATION
  end

  describe '.gitlab_org_group_member_usernames' do
    it_behaves_like 'cached group member attributes', :gitlab_org_group_member_usernames, :username, described_class::GITLAB_ORG_GROUP, described_class::GROUP_CACHE_DEFAULT_EXPIRATION
  end

  describe '.gitlab_com_group_member_ids' do
    it_behaves_like 'cached group member attributes', :gitlab_com_group_member_ids, :id, described_class::GITLAB_COM_GROUP, described_class::GROUP_CACHE_DEFAULT_EXPIRATION
  end

  describe '.gitlab_com_group_member_usernames' do
    it_behaves_like 'cached group member attributes', :gitlab_com_group_member_usernames, :username, described_class::GITLAB_COM_GROUP, described_class::GROUP_CACHE_DEFAULT_EXPIRATION
  end

  describe '.gitlab_com_appsec_member_ids' do
    it_behaves_like 'cached group member attributes', :gitlab_com_appsec_member_ids, :id, described_class::GITLAB_COM_APPSEC_GROUP, described_class::GROUP_CACHE_DEFAULT_EXPIRATION
  end

  describe '.gitlab_com_support_member_ids' do
    it_behaves_like 'cached group member attributes', :gitlab_com_support_member_ids, :id, described_class::GITLAB_COM_SUPPORT_GROUP, described_class::GROUP_CACHE_DEFAULT_EXPIRATION
  end

  describe '.jihu_team_member_ids' do
    it_behaves_like 'cached group member attributes', :jihu_team_member_ids, :id, described_class::JIHU_TEAM_MEMBER_GROUP, described_class::GROUP_CACHE_DEFAULT_EXPIRATION
  end

  describe '.user', :clean_cache do
    let(:user_id) { 42 }
    let(:user_response_1) { { 'id' => user_id, 'username' => 'a-user' } }
    let(:user_response_2) { { 'id' => user_id, 'username' => 'renamed-user' } }

    it 'caches the API response' do
      expect_api_request(path: "/users/#{user_id}", response_body: user_response_1) do
        # Populate the cache
        expect(described_class.user(user_id)).to eq(user_response_1)
      end

      # Next call is retrieved from cache
      expect(described_class.user(user_id)).to eq(user_response_1)

      # Travel 1 second after the cache expires
      Timecop.travel(Time.now + described_class::USER_CACHE_DEFAULT_EXPIRATION + 1) do
        expect_api_request(path: "/users/#{user_id}", response_body: user_response_2) do
          # Ensure the response is up-to-date
          expect(described_class.user(user_id)).to eq(user_response_2)
        end

        # Next call is retrieved from cache
        expect(described_class.user(user_id)).to eq(user_response_2)
      end
    end

    context 'when `fresh: true` is passed' do
      it 'calls the API and saves the response in the cache' do
        expect_api_request(path: "/users/#{user_id}", response_body: user_response_1) do
          # Populate the cache
          expect(described_class.user(user_id)).to eq(user_response_1)
        end

        expect_api_request(path: "/users/#{user_id}", response_body: user_response_2) do
          # Ensure the response is up-to-date
          expect(described_class.user(user_id, fresh: true)).to eq(user_response_2)
        end

        # Next call is retrieved from cache
        expect(described_class.user(user_id)).to eq(user_response_2)
      end
    end
  end

  describe '.dry_run?' do
    context 'when DRY_RUN is specified' do
      using RSpec::Parameterized::TableSyntax

      where(:dry_run_value, :expected) do
        'true' | true
        'True' | true
        'YES'  | true
        '1'    | true

        'no'            | false
        '0'             | false
        'anything else' | false
      end

      with_them do
        before do
          stub_env('DRY_RUN' => dry_run_value)
        end

        it 'is set to what is expected' do
          expect(described_class.dry_run?).to eq(expected)
        end
      end
    end

    it 'defaults to false' do
      expect(described_class.dry_run?).to be(false)
    end
  end
end
