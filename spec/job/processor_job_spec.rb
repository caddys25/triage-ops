# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/processor_job'

RSpec.describe Triage::ProcessorJob do
  include_context 'with event'

  let(:job)      { described_class.new }
  let(:handler)  { double('Triage::Handler') }
  let(:response) { job.perform(payload) }
  let(:status)   { response[0] }
  let(:body)     { response[2][0] }

  before do
    allow(Triage::Handler).to receive(:new).with(event).and_return(handler)
    allow(Triage::Event).to receive(:build).with(payload).and_return(event)
    allow(handler).to receive(:process)
  end

  context 'when Triage::Event::UnsupportedObjectKind is thrown' do
    it 'returns a 400 error with HTTP 200' do
      expect(handler).to receive(:process).and_raise(Triage::Event::UnsupportedObjectKind)

      expect(job).to receive(:log_warning).with(Triage::Event::UnsupportedObjectKind).and_call_original

      expect(body).to eq(JSON.dump(status: :error, status_code: 400, error: "Triage::Event::UnsupportedObjectKind", message: "Triage::Event::UnsupportedObjectKind"))
      expect(status).to eq(200)
    end
  end

  context 'when Triage::ClientError is thrown' do
    it 'returns a 400 error with HTTP 200' do
      expect(handler).to receive(:process).and_raise(Triage::ClientError)

      expect(job).to receive(:log_error).with(Triage::ClientError).and_call_original

      expect(body).to eq(JSON.dump(status: :error, status_code: 400, error: "Triage::ClientError", message: "Triage::ClientError"))
      expect(status).to eq(200)
    end
  end

  context 'when an error is thrown' do
    let(:error) { StandardError.new('runtime error') }

    before do
      allow(handler).to receive(:process).and_raise(error)
    end

    it 'returns a 500 error and tags Sentry error with event type with HTTP 200' do
      expect(Raven).to receive(:capture_exception).with(error)
      expect(Raven).to receive(:tags_context).with(event_class: 'Triage::Event', event_key: event.key, service: 'reactive')
      expect(Raven).to receive(:extra_context).with(processor: nil, payload: event.payload)
      expect(job.logger).to receive(:error).with(error, processor: nil, event_class: 'Triage::Event', event_key: event.key, event_payload: event.payload)

      expect(body).to eq(JSON.dump(status: :error, status_code: 500, error: "StandardError", message: "runtime error"))
      expect(status).to eq(200)
    end

    context 'when it is Exception' do
      let(:error) { Class.new(Exception).new } # rubocop:disable Lint/InheritException

      it 'logs and captures it and re-raises the exception' do
        expect(job).to receive(:log_error).with(error)
        expect(job).to receive(:capture_error).with(error)

        expect { response }.to raise_error(error)
      end
    end
  end

  context 'when no error is thrown' do
    let(:message) { 'foo' }
    let(:result) do
      {
        'processor' => double('Result', message: message, error: nil, duration: 42, to_h: {})
      }
    end

    before do
      allow(handler).to receive(:process).and_return(result)
    end

    it 'returns a 200 response with the messages from the processors' do
      expect(body).to eq(JSON.dump(status: :ok, messages: { 'processor' => message }))
      expect(status).to eq(200)
    end
  end

  context 'when the event is unknown' do
    let(:error) { Triage::Event::UnknownObjectKind.new('Unknown object') }

    before do
      allow(Triage::Event).to receive(:build).and_raise(error)
    end

    it 'returns a 200 response and logs the error' do
      expect(job.logger).to receive(:error).with(error, processor: nil, event_class: '', event_key: nil, event_payload: nil).ordered
      expect(body).to eq(JSON.dump(status: :error, status_code: 400, error: error.class.name, message: error.message))
      expect(status).to eq(200)
    end
  end

  context 'when there is a random error' do
    let(:error) { RuntimeError.new('Random error') }

    before do
      allow(handler).to receive(:process).and_raise(error)
    end

    it 'returns a 200 response and tags Sentry error with event type' do
      expect(job.logger).to receive(:error).with(error, processor: nil, event_class: 'Triage::Event', event_key: event.key, event_payload: event.payload).ordered
      expect(Raven).to receive(:tags_context).with(event_class: 'Triage::Event', event_key: event.key, service: 'reactive').ordered
      expect(Raven).to receive(:extra_context).with(processor: nil, payload: event.payload).ordered
      expect(Raven).to receive(:capture_exception).with(error).ordered

      expect(body).to eq(JSON.dump(status: :error, status_code: 500, error: error.class.name, message: error.message))
      expect(status).to eq(200)
    end
  end

  context 'when processors encountered errors' do
    let(:error1) { StandardError.new('runtime error 1') }
    let(:error2) { StandardError.new('runtime error 2') }
    let(:result) do
      {
        'processor1' => double('Result', message: 'first success', error: error1, duration: 42, to_h: {}),
        'processor2' => double('Result', message: nil, error: error2, duration: 42, to_h: {})
      }
    end

    before do
      allow(handler).to receive(:process).and_return(result)
    end

    it 'returns a 200 response and tags Sentry error with event type' do
      expect(job.logger).to receive(:error).with(error1, processor: 'processor1', event_class: 'Triage::Event', event_key: event.key, event_payload: event.payload).ordered
      expect(Raven).to receive(:tags_context).with(event_class: 'Triage::Event', event_key: event.key, service: 'reactive').ordered
      expect(Raven).to receive(:extra_context).with(processor: 'processor1', payload: event.payload).ordered
      expect(Raven).to receive(:capture_exception).with(error1).ordered

      expect(job.logger).to receive(:error).with(error2, processor: 'processor2', event_class: 'Triage::Event', event_key: event.key, event_payload: event.payload).ordered
      expect(Raven).to receive(:tags_context).with(event_class: 'Triage::Event', event_key: event.key, service: 'reactive').ordered
      expect(Raven).to receive(:extra_context).with(processor: 'processor2', payload: event.payload).ordered
      expect(Raven).to receive(:capture_exception).with(error2).ordered

      expect(body).to eq(JSON.dump(status: :ok, messages: { 'processor1' => 'first success' }))
      expect(status).to eq(200)
    end
  end
end
