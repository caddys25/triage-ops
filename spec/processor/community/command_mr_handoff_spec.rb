# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_handoff'

RSpec.describe Triage::CommandMrHandoff do
  include_context 'with discord posting context', 'DISCORD_WEBHOOK_PATH_COMMUNITY_MR_HANDOFF'
  include_context 'with event', Triage::MergeRequestNoteEvent do
    let(:event_attrs) do
      {
        new_comment: new_comment,
        url: url,
        title: title,
        project_path_with_namespace: project_path_with_namespace
      }
    end

    let(:new_comment) { %(#{Triage::GITLAB_BOT} handoff) }
    let(:url) { 'http://gitlab.com/mr_url' }
    let(:title) { 'MR Title' }
    let(:project_path_with_namespace) { 'gitlab-org/gitlab' }
  end

  let(:by_team_member) { false }
  let(:targets_public_project) { true }

  subject { described_class.new(event) }

  before do
    allow(event).to receive(:by_team_member?).and_return(by_team_member)
    allow(event).to receive(:project_public?).and_return(targets_public_project)
    allow(discord_messenger_stub).to receive(:send_message)
  end

  include_examples 'registers listeners', ['merge_request.note']
  include_examples 'command processor', 'handoff'

  describe '#applicable?' do
    include_context 'when command is a valid command from a wider community contribution'

    it_behaves_like 'community contribution command processor #applicable?'

    context 'when actor is not a team member' do
      it_behaves_like 'rate limited', count: 1, period: 3600
    end

    context 'when actor is a team member' do
      let(:by_team_member) { true }

      it_behaves_like 'rate limited', count: 100, period: 3600
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment to tell the community this MR is free to be picked up' do
      body = add_automation_suffix('processor/community/command_mr_handoff.rb') do
        <<~MARKDOWN.chomp
          We are looking for a community member to continue this MR.
          If you would like to pick it up, please leave a comment in here mentioning the Contributors Success Team with `@gitlab-org/developer-relations/contributor-success`.

          /label ~"#{Labels::WORKFLOW_IN_DEV_LABEL}" ~"#{Labels::SEEKING_COMMUNITY_CONTRIBUTIONS}"
          /unassign
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end

    context 'when posting messages to discord' do
      before do
        allow(subject).to receive(:add_comment)
      end

      it_behaves_like 'discord message posting' do
        let(:message_body) do
          <<~TEXT
            The previous assignee(s) can't work on a merge request anymore:
            - URL: #{event.url}
            - Title: `#{event.title}`
            - Project: `#{event.project_path_with_namespace}`
          TEXT
        end
      end

      context 'when merge request targets a non public project' do
        let(:targets_public_project) { false }

        it_behaves_like 'no discord message posting'
      end
    end
  end
end
