# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/workflow/notify_merged_master_broken_merge_request'

RSpec.describe Triage::Workflow::NotifyMergedMasterBrokenMergeRequest do
  include_context 'with slack posting context'
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        from_gitlab_org_gitlab?: from_gitlab_org_gitlab,
        label_names: label_names,
        object_kind: 'merge_request',
        title: mr_title,
        url: mr_url,
        action: action
      }
    end

    let(:mr_title) { 'MR title' }
    let(:mr_url) { 'https://mr.url' }
    let(:from_gitlab_org_gitlab) { true }
    let(:label_names) { [Labels::MASTER_BROKEN_LABEL] }
    let(:action) { 'open' }
  end

  subject { described_class.new(event, messenger: messenger_stub) }

  before do
    allow(messenger_stub).to receive(:ping)
  end

  include_examples 'registers listeners', ['merge_request.merge', 'merge_request.open', 'merge_request.approved']
  include_examples 'processor slack options', Triage::Workflow::NotifyMergedMasterBrokenMergeRequest::SLACK_CHANNEL

  describe '#applicable?' do
    include_examples 'event is applicable'

    context 'when event is not from gitlab-org/gitlab' do
      let(:from_gitlab_org_gitlab) { false }

      include_examples 'event is not applicable'
    end

    context 'when ~master:broken label is not set' do
      let(:label_names) { ['type::feature'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    context 'when an MR is opened' do
      let(:action) { 'open' }

      it_behaves_like 'slack message posting' do
        let(:message_body) do
          {
            text: <<~MARKDOWN
              A `~master:broken` MR was *opened* [#{mr_title}](#{mr_url})
            MARKDOWN
          }
        end
      end
    end

    context 'when an MR is merged' do
      let(:action) { 'merge' }

      it_behaves_like 'slack message posting' do
        let(:message_body) do
          {
            text: <<~MARKDOWN
              A `~master:broken` MR was *merged* [#{mr_title}](#{mr_url})
            MARKDOWN
          }
        end
      end
    end

    context 'when an MR is approved' do
      let(:action) { 'approved' }

      it_behaves_like 'slack message posting' do
        let(:message_body) do
          {
            text: <<~MARKDOWN
              A `~master:broken` MR was *approved* [#{mr_title}](#{mr_url})
            MARKDOWN
          }
        end
      end
    end
  end
end
