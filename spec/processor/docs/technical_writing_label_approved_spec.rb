# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/docs/technical_writing_label_approved'
require_relative 'technical_writing_label_shared_examples'

RSpec.describe Triage::Docs::TechnicalWritingLabelApproved do
  let(:action) { 'approval' }

  include_examples 'registers listeners', ['merge_request.approval', 'merge_request.approved']

  include_examples 'technical writing label features'

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end
end
