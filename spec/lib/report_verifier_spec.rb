# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/report_verifier'

RSpec.describe ReportVerifier do
  let(:policy_path) { 'policies/stages/report/untriaged-issues.yml' }
  let(:report_response) do
    TestResponse.new([{ 'title' => '2020-06-08 title' }])
  end

  let(:yaml_hash) do
    {
      'resource_rules' => {
        'issues' => {
          'rules' => [
            {
              name: 'rule_name',
              'actions' => {
                'summarize' => {
                  'destination' => 'project_path',
                  'title' => '#{date} title'
                }
              }
            }
          ]
        }
      }
    }
  end

  TestResponse = Struct.new(:parsed_response)

  before do
    allow(YAML).to receive(:load_file).and_return(yaml_hash)
    allow(HTTParty).to receive(:get).and_return(report_response)
  end

  subject { described_class.new(policy_path).verify }

  describe '#verify' do
    context 'with invalid policy' do
      let(:yaml_hash) do
        {
          'resource_rules' => {
            'issues' => {
              'rules' => [
                {
                  name: 'rule_name',
                  'actions' => {
                    'comment' => 'my comment'
                  }
                }
              ]
            }
          }
        }
      end

      it 'raises an argument error' do
        expect do
          subject
        end.to raise_error(ArgumentError)
      end
    end

    context 'with valid policy' do
      context 'with report' do
        context 'when that matches' do
          it { is_expected.to be(true) }
        end

        context 'when that does not match' do
          let(:report_response) do
            TestResponse.new([{ 'title' => '2020-06-08 different' }])
          end

          it { is_expected.to be(false) }
        end
      end

      context 'without report' do
        let(:report_response) do
          TestResponse.new([])
        end

        it { is_expected.to be(false) }
      end
    end
  end
end
