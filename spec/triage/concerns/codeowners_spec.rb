# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/concerns/codeowners'

RSpec.describe Triage::Codeowners, :clean_cache do
  let(:concrete_class) do
    Class.new do
      include Triage::Codeowners

      def self.name
        'ConcreteCodeownersClass'
      end
    end
  end

  let(:concrete_object) { concrete_class.new }
  let(:codeowners_url) { 'https://gitlab.com/foo/-/raw/master/.gitlab/CODEOWNERS' }
  let(:codeowners_body) do
    <<~CODEOWNERS
      # Lines that start with `#` are ignored.
      # Specify a default Code Owner by using a wildcard:
      * @default-codeowner

      *.rb @ruby/owner

      # Other ruby file with multiple owners
      otherfile.rb @multiple @code @owners @ruby/owner
      /path/to/files/ @group/owner
    CODEOWNERS
  end

  let(:owner) { '@code' }

  let(:codeowners_response) { double(parsed_response: codeowners_body) }

  before do
    allow(HTTParty).to receive(:get).and_return(codeowners_response)
  end

  shared_context 'with a valid CODEOWNERS URL' do
    before do
      concrete_class.const_set(:CODEOWNERS_FILE, codeowners_url)
    end
  end

  describe '#owners_for_path' do
    include_context 'with a valid CODEOWNERS URL'

    context 'when owner has a single matching pattern' do
      it 'returns a file pattern' do
        expect(concrete_object.paths_for_owner(owner)).to eq([
          "otherfile.rb"
        ])
      end
    end

    context 'when owner has multiple matching patterns' do
      let(:owner) { '@ruby/owner' }

      it 'returns multiple file patterns' do
        expect(concrete_object.paths_for_owner(owner)).to eq([
          "*.rb",
          "otherfile.rb"
        ])
      end
    end
  end

  describe '#codeowners_data' do
    context 'when CODEOWNERS_URL_VAR is not defined' do
      it 'raises an exception when trying to get codeowners_url' do
        expect { concrete_object.codeowners_data }.to raise_error(NameError)
      end
    end

    context 'when CODEOWNERS_URL_VAR is set' do
      include_context 'with a valid CODEOWNERS URL'

      context 'when CODEOWNERS_URL_VAR is not a valid URL' do
        let(:codeowners_url) { 'foo bar' }

        it 'raises an exception when trying to get codeowners_url' do
          expect { concrete_object.codeowners_data }.to raise_error(described_class::CodeownersUrlMissingError)
        end
      end

      context 'when CODEOWNERS_URL_VAR has the wrong host' do
        let(:codeowners_url) { 'foo.org/api/gitlab/bar' }

        it 'raises an exception when trying to get codeowners_url' do
          expect { concrete_object.codeowners_data }.to raise_error(described_class::CodeownersUrlInvalidError)
        end
      end

      context 'when CODEOWNERS_URL_VAR is valid' do
        it 'parses the CODEOWNERS data' do
          expect(concrete_object.codeowners_data).to eq({
            "*" => ["@default-codeowner"],
            "*.rb" => ["@ruby/owner"],
            "otherfile.rb" => ["@multiple", "@code", "@owners", "@ruby/owner"],
            "/path/to/files/" => ["@group/owner"]
          })
        end
      end

      it 'caches the CODEOWNERS URL response', :clean_cache do
        concrete_object.codeowners_data

        expect(HTTParty).not_to receive(:get)

        concrete_object.codeowners_data
      end

      it 'expires the cache and retrieves', :clean_cache do
        concrete_object.codeowners_data

        # expire cache
        Timecop.travel(Time.now + described_class::CODEOWNERS_CACHE_EXPIRY + 1) do
          expect(HTTParty).to receive(:get)

          concrete_object.codeowners_data
        end
      end
    end
  end
end
