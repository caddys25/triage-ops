# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/concerns/graphql_query'

RSpec.describe Triage::GraphqlQuery, :clean_cache do
  let(:concrete_class) do
    Class.new do
      include Triage::GraphqlQuery

      def self.name
        'ConcreteGraphQLClass'
      end
    end
  end

  let(:concrete_object) { concrete_class.new }

  let(:query_result) { { "group" => { "mergeRequests" => { "count" => 12 } } } }
  let(:reduced_query_result) { { "mergeRequests" => { "count" => 12 } } }
  let(:client) { instance_double(GraphQL::Client, query: response, parse: nil) }
  let(:errors) { [] }
  let(:response) do
    double(errors: errors, data: response_data, group: reduced_query_result, extensions: { 'headers' => { 'ratelimit-remaining' => 600, 'ratelimit-reset' => 1604942574 } })
  end

  let(:response_data) { nil }

  let(:graphql_query) do
    <<-'GRAPHQL'
    query {
      group(fullPath: "gitlab-org") {
        mergeRequests(authorUsername: "testUser",
        state: merged, includeSubgroups: true) {
          count
        }
      }
    }
    GRAPHQL
  end

  describe '#query' do
    before do
      allow(concrete_object).to receive(:fetch_schema).and_return('GraphQLSchema')
      allow(concrete_object).to receive(:client).and_return(client)
    end

    context 'with a valid query' do
      let(:response_data) { { "group" => { "mergeRequests" => { "count" => 12 } } } }

      it 'returns the query result' do
        expect(concrete_object.query(graphql_query)).to eq(query_result)
      end

      context 'with a resource_path' do
        let(:response_data) { double(group: { "mergeRequests" => { "count" => 12 } }) }
        let(:query_reduction) { ["group"] }

        it 'returns the modified result' do
          expect(concrete_object.query(graphql_query, resource_path: query_reduction)).to eq(reduced_query_result)
        end
      end

      context 'with a deep resource_path' do
        let(:response_data) { double(group: double(merge_requests: double(count: 12))) }
        let(:query_reduction) { %w[group merge_requests count] }

        it 'returns a simple result' do
          expect(concrete_object.query(graphql_query, resource_path: query_reduction)).to eq(12)
        end
      end
    end

    context 'when there is a server error' do
      let(:errors) { { errors: [{ message: "500 error" }] } }

      it 'returns an error response' do
        expect(concrete_object.query(graphql_query)).to eq({ errors: [{ message: "500 error" }] })
      end
    end
  end

  describe '#schema' do
    context 'when the schema is queried' do
      before do
        stub_request(:post, 'https://gitlab.com/api/graphql')
        .with(body: read_fixture('graphql/graphql_schema_query.json'))
        .to_return(status: 200, body: read_fixture('graphql/graphql_schema.json'), headers: {})
      end

      it 'fetches a valid schema', :clean_cache do
        schema = concrete_object.schema

        expect(schema.respond_to?(:directives)).to be_truthy
      end

      context 'with an http error' do
        it 'raises a KeyError' do
          allow(HTTParty).to receive(:post).and_return(
            double(
              code: 500,
              message: { errors: [{ message: '500 error' }] }.to_json
            )
          )

          expect { concrete_object.schema }.to raise_error(KeyError)
        end
      end

      it 'caches the schema response', :clean_cache do
        allow(GraphQL::Client).to receive(:load_schema).and_return('GraphQLSchema')

        concrete_object.schema

        expect(concrete_object).not_to receive(:fetch_schema)

        concrete_object.schema
      end

      it 'expires the cache and retrieves a fresh schema', :clean_cache do
        allow(concrete_object).to receive(:fetch_schema).and_return('GraphQLSchema')

        concrete_object.schema

        # expire cache
        Timecop.travel(Time.now + described_class::GRAPHQL_CACHE_EXPIRY + 1) do
          expect(concrete_object).to receive(:fetch_schema).and_return('GraphQLSchema')

          concrete_object.schema
        end
      end
    end
  end
end
