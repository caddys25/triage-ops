# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/processor/react_to_changes'
require_relative '../../../triage/triage/react_to_changes/notify_feature_flag_mr_deployment'

RSpec.describe Triage::NotifyFeatureFlagMrDeployment do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:env_label) { 'workflow::staging-canary' }
    let(:added_label_names) { [env_label] }
    let(:label_names) { added_label_names }
    let(:mr_state) { 'merged' }
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        from_gitlab_org_gitlab?: true,
        label_names: label_names,
        added_label_names: added_label_names,
        state: mr_state
      }
    end

    let(:feature_flag_default_enabled) { 'true' }
    let(:feature_flag_config_change) do
      {
        'old_path' => 'config/feature_flags/ops/api.yml',
        'new_path' => 'config/feature_flags/ops/api.yml',
        'diff' => "+ default_enabled: #{feature_flag_default_enabled}"
      }
    end

    let(:merge_request_changes) do
      { 'changes' => [feature_flag_config_change].compact }
    end

    let(:feature_flag_definition_file_content) do
      <<~YAML.chomp
      ---
      name: usage_data_api
      introduced_by_url: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41301
      rollout_issue_url: https://gitlab.com/gitlab-org/gitlab/-/issues/267114
      milestone: '13.4'
      type: ops
      group: group::analytics instrumentation
      default_enabled: #{feature_flag_default_enabled}
      YAML
    end
  end

  let(:processor) { Triage::ReactToChanges.new(event) }

  subject { described_class.new(processor) }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{iid}/changes",
      response_body: merge_request_changes)
    stub_api_request(
      path: "/projects/#{project_id}/repository/files/#{Triage.api_client.url_encode(feature_flag_config_change&.fetch('new_path', ''))}/raw?ref=master",
      response_headers: { 'content-type' => 'text/plain; charset=utf-8' },
      response_body: feature_flag_definition_file_content)
  end

  it_behaves_like 'processor documentation is present'

  describe '#applicable?' do
    include_examples 'applicable on contextual event'

    context 'when event project is not under gitlab-org/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    %w[opened closed].each do |state|
      context "when MR is #{state}" do
        let(:mr_state) { state }

        include_examples 'event is not applicable'
      end
    end

    context 'when MR does not have environment labels added' do
      let(:added_label_names) { ['foo'] }

      include_examples 'event is not applicable'
    end

    context 'when there is no feature flag config changes' do
      let(:feature_flag_config_change) { nil }

      include_examples 'event is not applicable'
    end
  end

  describe '#react' do
    let(:base_path) { '/projects/123/merge_requests/456' }
    let(:discussion_id) { 729 }

    shared_examples 'adds a relevant comment' do |expected_second_sentence|
      let(:message) do
        <<~MESSAGE.strip
        <!-- triage-serverless NotifyFeatureFlagMrDeployment -->
        :rocket: @joe This merge request was deployed to the ~"#{env_label}" environment.
        #{expected_second_sentence}
        MESSAGE
      end

      context 'when no discussion exist yet' do
        it 'creates a new discussion' do
          expect_api_requests do |requests|
            requests << stub_api_request(path: "#{base_path}/discussions", response_body: [], query: { per_page: 100 })
            requests << stub_api_request(verb: :post, path: "#{base_path}/discussions", request_body: { 'body' => message })

            subject.react
          end
        end
      end

      context 'when a discussion already exists' do
        it 'adds a comment to the existing discussion' do
          expect_api_requests do |requests|
            requests << stub_api_request(path: "#{base_path}/discussions", response_body: [{ 'id' => discussion_id, 'notes' => [{ 'body' => message }] }], query: { per_page: 100 })
            requests << stub_api_request(verb: :post, path: "#{base_path}/discussions/#{discussion_id}/notes", request_body: { 'body' => message })

            subject.react
          end
        end
      end
    end

    shared_examples 'adds a comment to delete the feature flag record in the relevant environment' do |expected_env_arg|
      it_behaves_like 'adds a relevant comment',
        "You may want to delete the associated feature flag record on this environment with `/chatops run feature delete usage_data_api --dev --ops --pre --#{expected_env_arg}`."
    end

    shared_examples 'adds a comment to enable the feature flag record in the relevant environment' do |expected_env_arg|
      it_behaves_like 'adds a relevant comment',
        "You may want to enable the associated feature flag on this environment with `/chatops run feature set usage_data_api true --#{expected_env_arg}`."
    end

    shared_examples 'adds a comment to delete the unknown feature flag record in the relevant environment' do |expected_env_arg|
      it_behaves_like 'adds a relevant comment',
        "You may want to delete the associated feature flag record on this environment with `/chatops run feature delete <could not retrieve ff name> --dev --ops --pre --#{expected_env_arg}`."
    end

    context 'when feature flag definition file cannot be found' do
      let(:feature_flag_definition_file_content) { '' }

      it_behaves_like 'adds a comment to delete the unknown feature flag record in the relevant environment', 'staging'
    end

    context 'when default_enabled is set to true' do
      %w[staging staging-canary].each do |env|
        context "when deployed to #{env}" do
          let(:env_label) { "workflow::#{env}" }

          it_behaves_like 'adds a comment to delete the feature flag record in the relevant environment', 'staging'
        end
      end

      %w[production canary].each do |env|
        context "when deployed to #{env}" do
          let(:env_label) { "workflow::#{env}" }

          it_behaves_like 'adds a comment to delete the feature flag record in the relevant environment', 'production'
        end
      end
    end

    context 'when default_enabled is set to false' do
      let(:feature_flag_default_enabled) { 'false' }

      %w[staging staging-canary].each do |env|
        context "when deployed to #{env}" do
          let(:env_label) { "workflow::#{env}" }

          it_behaves_like 'adds a comment to enable the feature flag record in the relevant environment', 'staging'
        end
      end

      %w[production canary].each do |env|
        context "when deployed to #{env}" do
          let(:env_label) { "workflow::#{env}" }

          it_behaves_like 'adds a comment to enable the feature flag record in the relevant environment', 'production'
        end
      end
    end
  end
end
