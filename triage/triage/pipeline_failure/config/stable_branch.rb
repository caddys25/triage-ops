# frozen_string_literal: true

require_relative 'base'

module Triage
  module PipelineFailure
    module Config
      class StableBranch < Base
        INCIDENT_PROJECT_ID = '5064907' # gitlab-org/release/tasks
        INCIDENT_LABELS = %w[release-blocker stable-branch-failure].freeze
        INCIDENT_TEMPLATE = <<~MARKDOWN.freeze
          #{DEFAULT_INCIDENT_SUMMARY_TABLE}

          ### General guidelines

          A broken stable branch prevents patch releases from being built.
          Fixing the pipeline is a priority to prevent any delays in releases.

          The process in the [Broken `master` handbook guide](https://about.gitlab.com/handbook/engineering/workflow/#broken-master) can be referenced since much of that process also applies here.

          ### Investigation

          **Be sure to fill the `Timeline` for this incident.**

          1. If the failure is new, and looks like a potential flaky failure, you can retry the failing job.
            Make sure to mention the retry in the `Timeline` and leave a link to the retried job.
          1. Search for similar master-broken issues in https://gitlab.com/gitlab-org/quality/engineering-productivity/master-broken-incidents/-/issues
            1. If one exists, ask the DRI of the master-broken issue to cherry-pick any resulting merge requests into the stable branch

          @gitlab-org/release/managers if the merge request author or maintainer is not available, this can be escalated using the dev-on-call process in the [#dev-escalation slack channel](https://gitlab.slack.com/archives/CLKLMSUR4).

          ### Pre-resolution

          If you believe that there's an easy resolution by either:

          - Reverting a particular merge request.
          - Making a quick fix (for example, one line or a few similar simple changes in a few lines).
            You can create a merge request, assign to any available maintainer, and ping people that were involved/related to the introduction of the failure.
            Additionally, a message can be posted in `#backend_maintainers` or `#frontend_maintainers` to get a maintainer take a look at the fix ASAP.
          - Cherry picking a change that was used to fix a similar master-broken issue.

          ### Resolution

          Add a comment to this issue describing how this incident could have been prevented earlier in the Merge Request pipeline (rather than the merge commit pipeline).
        MARKDOWN

        SLACK_CHANNEL = 'releases'

        def self.match?(event)
          event.on_instance?(:com) &&
            event.project_path_with_namespace == 'gitlab-org/gitlab' &&
            event.ref.match?(/^[\d-]+-stable(-ee)?$/)
        end

        def incident_project_id
          INCIDENT_PROJECT_ID
        end

        def incident_template
          INCIDENT_TEMPLATE
        end

        def incident_labels
          INCIDENT_LABELS
        end

        def incident_extra_attrs
          { assignee_ids: assignee_ids }
        end

        def default_slack_channels
          [SLACK_CHANNEL]
        end

        private

        def assignee_ids
          ids = [event.event_actor.id]
          ids << event.merge_request['author']['id'].to_i if event.merge_request
          ids
        end
      end
    end
  end
end
