# frozen_string_literal: true

require_relative 'base'

module Triage
  module PipelineFailure
    module Config
      class Ruby2Branch < Base
        SLACK_CHANNEL = 'backend'

        def self.match?(event)
          event.on_instance?(:com) &&
            event.project_path_with_namespace == 'gitlab-org/gitlab' &&
            event.ref == 'ruby2' &&
            event.source_job_id.nil?
        end

        def default_slack_channels
          [SLACK_CHANNEL]
        end
      end
    end
  end
end
