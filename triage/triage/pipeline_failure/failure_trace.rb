# frozen_string_literal: true

require_relative '../../../lib/constants/labels'

module Triage
  module PipelineFailure
    class FailureTrace
      FAILURE_TRACE_MARKERS = {
        rspec: { start: "Failures:\n", end: "\n[TEST PROF INFO]", language: 'ruby', label: '~backend' },
        jest: { start: "Summary of all failing tests\n", end: "\nRan all test suites.", language: 'javascript', label: '~frontend' },
        workhorse: {
          start: "make: Entering directory '/builds/gitlab-org/gitlab/workhorse'",
          end: "make: Leaving directory '/builds/gitlab-org/gitlab/workhorse'",
          language: 'go',
          label: '~workhorse'
        }
      }.freeze

      FAILURE_TRACE_SIZE_LIMIT = 50000 # Keep it processable by human
      SOURCE_CODE_GROUP = "group::source code"

      attr_reader :job_name, :job_trace, :config, :framework, :failed_examples_data

      def initialize(job_name:, job_trace:, config:, failed_examples_data:)
        @job_name = job_name
        @job_trace = job_trace
        @config = config
        @framework = identify_framework
        @failed_examples_data = failed_examples_data || []
      end

      def summary_markdown
        return unless framework

        <<~MARKDOWN.chomp
        ```#{markers[:language]}
        #{failure_summary[...FAILURE_TRACE_SIZE_LIMIT]}
        ```

        /label #{markers[:label]}
        MARKDOWN
      end

      def attribution_message_markdown
        message = ''

        if framework == :rspec
          message = failed_examples_data.map do |example|
            "- ~\"#{example['product_group_label']}\" ~\"#{example['feature_category_label']}\" #{example['id']}"
          end.join("\n")
        elsif framework == :workhorse
          message = "- ~\"#{SOURCE_CODE_GROUP}\" #{job_name}"
        end

        <<~MARKDOWN.chomp
          #{message}
        MARKDOWN
      end

      def root_cause_label
        label_key = config.root_cause_to_trace_map.detect do |_root_cause, trace_patterns|
          trace_patterns.any? { |pattern| job_trace.downcase.include?(pattern.downcase) }
        end&.first || :default

        Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS.fetch(label_key)
      end

      def top_group_label
        potential_group_labels
          .tally
          .max_by { |_label, count| count }[0]
      end

      def potential_group_labels
        @potential_group_labels ||=
          case framework
          when :workhorse
            [SOURCE_CODE_GROUP]
          when :rspec
            failed_examples_data.filter_map { |example| example['product_group_label'] }
          else
            []
          end
      end

      private

      def identify_framework
        FAILURE_TRACE_MARKERS.each do |framework, markers|
          return framework if job_trace.include?(markers[:start]) && job_trace.include?(markers[:end])
        end

        nil
      end

      def failure_summary
        job_trace.split(markers[:start]).last.split(markers[:end]).first.chomp
      end

      def markers
        FAILURE_TRACE_MARKERS.fetch(framework)
      end
    end
  end
end
