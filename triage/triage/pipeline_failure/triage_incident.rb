# frozen_string_literal: true

require_relative '../../../triage/triage'
require_relative '../../../triage/resources/ci_job'
require_relative '../../../lib/constants/labels'
require_relative '../../../lib/www_gitlab_com'
require_relative 'failure_trace'
require_relative 'pipeline_incident_finder'

module Triage
  module PipelineFailure
    class TriageIncident
      ROOT_CAUSE_LABELS = Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS
      LABELS_FOR_TRANSIENT_ERRORS = Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS.values_at(
        :failed_to_pull_image,
        :gitlab_com_overloaded,
        :runner_disk_full,
        :infrastructure,
        :job_timeout
      ).freeze

      POST_RETRY_JOB_URL_THRESHOLD = 10
      FAILURE_TRACE_SIZE_LIMIT = 50000 # Keep it processable by human

      attr_reader :event, :config, :failed_jobs, :ci_jobs

      def initialize(event:, config:, failed_jobs:)
        @event = event
        @config = config
        @failed_jobs = failed_jobs
        @ci_jobs = failed_jobs.to_h do |job|
          [job.id, Triage::CiJob.new(instance: event.instance, project_id: event.project_id, job_id: job.id)]
        end
      end

      def top_root_cause_label
        return ROOT_CAUSE_LABELS[:default] if triaged_jobs.empty?

        # find the top root cause label preferably not master-broken::undetermined
        potential_root_cause_labels
          .tally
          .max_by { |label, count| label == ROOT_CAUSE_LABELS[:default] ? 0 : count }[0]
      end

      def top_group_label
        return if triaged_jobs.empty? || potential_group_labels.empty?

        potential_group_labels
          .tally
          .max_by { |_label, count| count }[0]
      end

      def root_cause_analysis_comment
        return if triaged_jobs.empty?

        [
          list_all_triaged_jobs_comment,
          retry_pipeline_comment,
          close_incident_comment
        ].compact.join("\n\n").prepend("\n\n")
      end

      def investigation_comment
        return if triaged_jobs.empty? || closeable?

        jobs_with_failed_tests = triaged_jobs.reject { |triaged_job| triaged_job[:failed_test_trace].nil? }
        return if jobs_with_failed_tests.empty?

        jobs_with_failed_tests.map do |triaged_job|
          <<~MARKDOWN.chomp
            - #{triaged_job[:link]}:

            #{triaged_job[:failed_test_trace]}
          MARKDOWN
        end.join("\n\n").prepend("\n\n")
      end

      def attribution_comment
        return if triaged_jobs.empty?

        message_body = triaged_jobs.map do |triaged_job|
          <<~MARKDOWN.chomp
            #{triaged_job[:attribution_message]}
          MARKDOWN
        end.uniq.reject(&:empty?).join("\n\n").prepend("\n\n")

        message_body += attributed_slack_channel_info if attributed_slack_channel_info.present?
        message_body
      end

      def duplicate_incident_iid
        previous_incident = PipelineIncidentFinder.new(incident_project_id: config.incident_project_id).latest_incident

        return unless previous_incident

        # if the previous incident was closed as a duplicate of another incident
        # and the current incident has the same failed job names as the previous incident
        # we mark the current incident as a duplicate as well
        previous_failed_job_names = previous_incident.title.split('with ').last.split(', ')
        current_failed_job_names = failed_jobs.map(&:name)
        return unless previous_failed_job_names.sort == current_failed_job_names.sort

        closed_as_duplicate_of_iid(previous_incident)
      end

      def duplicate_command_body
        <<~MARKDOWN.chomp.prepend("\n\n")
        Closing as a duplicate of incident ##{duplicate_incident_iid}. Please reopen it if you don't think this is a duplicate.

        /copy_metadata ##{duplicate_incident_iid}
        /duplicate ##{duplicate_incident_iid}
        MARKDOWN
      end

      private

      def list_all_triaged_jobs_comment
        return if triaged_jobs.empty?

        triaged_jobs.map do |triaged_job|
          %(- #{triaged_job[:link]}: ~"#{triaged_job[:label]}".#{retry_job_comment(triaged_job)})
        end.join("\n")
      end

      def triaged_jobs
        @triaged_jobs ||=
          failed_jobs.map do |job|
            failure_trace = FailureTrace.new(
              job_name: job.name,
              job_trace: ci_jobs.fetch(job.id).trace,
              failed_examples_data: ci_jobs.fetch(job.id).failed_rspec_test_metadata,
              config: config
            )

            {
              id: job.id,
              link: "[#{job.name}](#{job.web_url})",
              label: failure_trace.root_cause_label,
              failed_test_trace: failure_trace.summary_markdown,
              attribution_labels: failure_trace.potential_group_labels,
              attribution_message: failure_trace.attribution_message_markdown
            }
          end
      end

      def retry_job_comment(failed_job)
        retry_job_web_url = retry_job_and_return_web_url_if_applicable(failed_job)
        return if retry_job_web_url.nil?

        " Retried at: #{retry_job_web_url}"
      end

      def retry_job_and_return_web_url_if_applicable(triaged_job)
        return unless transient_error?(triaged_job[:label]) && retry_jobs_individually?

        ci_jobs.fetch(triaged_job[:id]).retry['web_url']
      end

      def transient_error?(root_cause_label)
        LABELS_FOR_TRANSIENT_ERRORS.include?(root_cause_label)
      end

      def retry_jobs_individually?
        # the benefit of retrying individual job is so we can post the retried job url
        # but if there are too many failed jobs
        # it's more practical to click `retry pipeline` and verify the overall pipeline status when all jobs finish
        failed_jobs.size < POST_RETRY_JOB_URL_THRESHOLD
      end

      def retry_pipeline_comment
        return unless all_jobs_failed_with_transient_errors?
        return if retry_jobs_individually?

        retry_pipeline_response = Triage.api_client.retry_pipeline(event.project_id, event.id).to_h

        "Retried pipeline: #{retry_pipeline_response['web_url']}"
      end

      def close_incident_comment
        return unless closeable?

        "This incident is caused by known transient error(s), closing."
      end

      def all_jobs_failed_with_transient_errors?
        return @all_jobs_failed_with_transient_errors if defined?(@all_jobs_failed_with_transient_errors)

        @all_jobs_failed_with_transient_errors =
          if triaged_jobs.empty?
            false
          else
            (potential_root_cause_labels.uniq - config.transient_root_cause_labels).empty?
          end
      end
      alias_method :closeable?, :all_jobs_failed_with_transient_errors?
      public :closeable?

      def potential_group_labels
        @potential_group_labels ||= (triaged_jobs.map { |job| job[:attribution_labels] }).flatten
      end

      def closed_as_duplicate_of_iid(incident)
        duplicate_link = incident._links['closed_as_duplicate_of']
        return unless duplicate_link

        duplicate_link.split('/').last.to_i
      end

      def potential_root_cause_labels
        @potential_root_cause_labels ||= triaged_jobs.map { |job| job[:label] }
      end

      def groups
        @groups ||= WwwGitLabCom.groups
      end

      def top_group
        return unless top_group_label

        _group_key, group_data = groups.find { |_k, group| group['label'] == top_group_label }
        return unless group_data

        group_data
      end

      def top_group_channel
        return unless top_group

        top_group['slack_channel']
      end

      def attributed_slack_channel_info
        return unless top_group_channel

        <<~MARKDOWN.chomp.prepend("\n\n")
        **This incident is attributed to ~"#{top_group_label}" and posted in `##{top_group_channel}`.**
        MARKDOWN
      end
    end
  end
end
