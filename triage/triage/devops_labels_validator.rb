# frozen_string_literal: true
require_relative '../triage'
require_relative '../triage/event'
require_relative '../../lib/devops_labels'
require_relative '../../lib/constants/labels'
require_relative '../resources/issue'

module Triage
  class DevopsLabelsValidator
    attr_reader :label_names

    def initialize(label_names)
      @label_names = label_names
    end

    def labels_set?
      special_team_labels? || has_group_label? || has_category_label?
    end

    private

    def special_team_labels?
      (label_names & Labels::SPECIAL_TEAM_LABELS).flatten.any?
    end

    def has_group_label?
      label_names.any? { |label_name| label_name.start_with?('group::') }
    end

    def has_category_label?
      label_names.any? { |label_name| label_name.start_with?('Category:') }
    end
  end
end
