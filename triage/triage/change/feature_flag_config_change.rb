# frozen_string_literal: true

require_relative '../change'

module Triage
  class FeatureFlagConfigChange < Change
    def self.file_patterns
      # See https://gitlab.com/gitlab-org/gitlab/-/blob/7565bc6a7d36d00ead0b4d3e2fb6299284fcb063/.gitlab/ci/rules.gitlab-ci.yml#L584
      # Look for .feature-flag-development-config-patterns
      @file_patterns ||=
        %r{ ^(?:ee/|jh/)?config/feature_flags/(?:development|ops)/.*\.yml$
          }x
    end
  end
end
