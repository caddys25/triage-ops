# frozen_string_literal: true

require_relative 'feature_flag_config_change'

module Triage
  class FeatureFlagDefaultEnabledTrueChange < FeatureFlagConfigChange
    def self.line_patterns
      @line_patterns ||= /^\+\s*default_enabled:\s*true/
    end
  end
end
