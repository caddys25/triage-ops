# frozen_string_literal: true

require 'mini_cache'
require 'gitlab'

module Triage
  GITLAB_INSTANCE_ENDPOINTS = {
    com: ENV.fetch('GITLAB_API_ENDPOINT', 'https://gitlab.com'),
    dev: 'https://dev.gitlab.org'
  }.freeze
  GITLAB_BOT                     = '@gitlab-bot'
  GITLAB_ORG_GROUP               = 'gitlab-org'
  GITLAB_COM_GROUP               = 'gitlab-com'
  GITLAB_ORG_CONTRACTORS_GROUP   = 'gitlab-org/contractors'
  GITLAB_ORG_SECURITY_GROUP      = 'gitlab-org/security'
  GITLAB_COM_APPSEC_GROUP        = 'gitlab-com/gl-security/appsec'
  GITLAB_COM_SUPPORT_GROUP       = 'gitlab-com/support'
  GITLAB_DOCS_TEAM               = 'gl-docsteam'
  JIHU_TEAM_MEMBER_GROUP         = 'gitlab-jh/jh-team'
  GROUP_CACHE_DEFAULT_EXPIRATION = 3600 # 1 hour
  USER_CACHE_DEFAULT_EXPIRATION  = 3600 * 6 # 6 hours
  DEFAULT_ASYNC_DELAY_MINUTES    = 60 * 5 # 5 minutes
  GROUP_CACHE = {
    GITLAB_ORG_GROUP => [
      {
        name: :gitlab_org_group_member_ids,
        attr: :id
      },
      {
        name: :gitlab_org_group_member_usernames,
        attr: :username
      }
    ],
    GITLAB_COM_GROUP => [
      {
        name: :gitlab_com_group_member_ids,
        attr: :id
      },
      {
        name: :gitlab_com_group_member_usernames,
        attr: :username
      }
    ],
    GITLAB_COM_APPSEC_GROUP => [
      {
        name: :gitlab_com_appsec_member_ids,
        attr: :id
      }
    ],
    GITLAB_COM_SUPPORT_GROUP => [
      {
        name: :gitlab_com_support_member_ids,
        attr: :id
      }
    ],
    GITLAB_DOCS_TEAM => [
      {
        name: :gitlab_docs_team_member_ids,
        attr: :id
      }
    ],
    GITLAB_ORG_CONTRACTORS_GROUP => [
      {
        name: :gitlab_org_contractors_member_ids,
        attr: :id
      }
    ],
    JIHU_TEAM_MEMBER_GROUP => [
      {
        name: :jihu_team_member_ids,
        attr: :id
      }
    ]
  }.freeze
  GROUP_MEMBER_ATTRS_CACHEABLE = %i[id username].freeze

  def self.current_monotonic_time
    Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second)
  end

  def self.dry_run?
    (ENV['DRY_RUN'] || '0').match?(/yes|1|true/i)
  end

  def self.cache
    @cache ||= MiniCache::Store.new
  end

  def self.api_endpoint(instance_slug = :com)
    cache.get_or_set("api_endpoint_#{instance_slug}") do
      "#{GITLAB_INSTANCE_ENDPOINTS.fetch(instance_slug)}/api/v4"
    end
  end

  def self.api_token(instance_slug = :com)
    cache.get_or_set("api_token_#{instance_slug}") do
      ENV.fetch("GITLAB_#{instance_slug}_API_TOKEN".upcase).to_s
    end
  end

  def self.api_client(instance_slug = :com)
    cache.get_or_set("api_client_#{instance_slug}") do
      Gitlab.client(endpoint: api_endpoint(instance_slug), private_token: api_token(instance_slug))
    end
  end

  class << self
    GROUP_CACHE.each do |group, cache_definitions|
      cache_definitions.each do |definition|
        define_method(definition.fetch(:name)) do |fresh: false|
          group_member_attrs(
            group,
            definition.fetch(:attr),
            fresh: fresh,
            expires_in: definition.fetch(:expires_in, GROUP_CACHE_DEFAULT_EXPIRATION))
        end
      end
    end
  end

  def self.group_member_attrs(group, attr, fresh:, expires_in:)
    cache_method = fresh ? :set : :get_or_set
    # Make sure we have the up-to-date source data
    cached_group_members = group_members(group, fresh: fresh, expires_in: expires_in)

    cache.public_send(cache_method, "#{group}_group_member_#{attr}s".to_sym) do
      # Don't set an expiration time here as these caches are cleared in `.group_members`.
      cached_group_members.map(&attr)
    end
  end
  private_class_method :group_member_attrs

  def self.group_members(group, fresh:, expires_in:)
    cache_method = fresh ? :set : :get_or_set

    cache.public_send(cache_method, "#{group}_group_members".to_sym, expires_in: expires_in) do
      group_member_attrs_cache_keys(group).each { |cache_key| cache.unset(cache_key) }
      fresh_group_members(group)
    end
  end
  private_class_method :group_members

  def self.group_member_attrs_cache_keys(group)
    GROUP_MEMBER_ATTRS_CACHEABLE.map { |attr| "#{group}_group_member_#{attr}s".to_sym }
  end

  def self.fresh_group_members(group)
    api_client
      .group_members(group, per_page: 100)
      .auto_paginate
  end
  private_class_method :fresh_group_members

  def self.user(user_id, fresh: false)
    cache_method = fresh ? :set : :get_or_set

    cache.public_send(cache_method, "user-#{user_id}".to_sym, expires_in: USER_CACHE_DEFAULT_EXPIRATION) do
      api_client.user(user_id).to_h
    end
  end
end
