# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../../lib/constants/labels'

module Triage
  class BrokenMasterLabelNudger < Processor
    react_to 'incident.close'

    THIRTY_SECONDS = 30

    def applicable?
      event.from_master_broken_incidents_project? &&
        has_master_broken_label? &&
        !event.gitlab_bot_event_actor? &&
        (need_root_cause_label? || need_flaky_reason_label?)
    end

    def process
      BrokenMasterLabelNudgerJob.perform_in(THIRTY_SECONDS, event)
    end

    def documentation
      <<~TEXT
        This processor reminds team member to provide concrete master-broken root cause label or flaky test reason label.
      TEXT
    end

    private

    def has_master_broken_label?
      event.label_names.include?(Labels::MASTER_BROKEN_LABEL) || event.label_names.include?(Labels::MASTER_FOSS_BROKEN_LABEL)
    end

    def need_root_cause_label?
      event.label_names.include?(Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS[:default])
    end

    def need_flaky_reason_label?
      event.label_names.include?(Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS[:flaky_test]) && !has_flaky_reason_label?
    end

    def has_flaky_reason_label?
      event.label_names.any? { |label| label.start_with? Labels::FLAKY_TEST_LABEL_PREFIX }
    end
  end
end
