# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../../lib/team_member_select_helper'
require_relative '../../../lib/constants/labels'

module Triage
  class AssignComplianceDevForVerification < Processor
    include TeamMemberSelectHelper

    react_to 'issue.update'

    def applicable?
      event.from_gitlab_org? &&
        has_valid_labels? && one_assignee?
    end

    def process
      sleep(rand * 3) # Attempt to reduce race

      assign_team_member_for_verification if still_one_assignee?
    end

    def documentation
      <<~TEXT
        This processor assigns a dev from the configured group to the updated issue
        when the issue does not have any assignees and moved to `workflow::verification`.
      TEXT
    end

    private

    def has_valid_labels?
      has_verification_label? && has_group_label? && !has_verified_by_author_label?
    end

    def has_verification_label?
      label_names.include?(Labels::WORKFLOW_VERIFICATION)
    end

    def has_group_label?
      ([Labels::COMPLIANCE_GROUP_LABEL] & label_names).any?
    end

    def has_verified_by_author_label?
      label_names.include?(Labels::VERIFIED_BY_AUTHOR)
    end

    def one_assignee?
      event.assignee_ids.count == 1
    end

    def still_one_assignee?
      issue.present? && issue.state == 'opened' && issue.assignees.count == 1
    end

    def issue
      @issue ||= Triage.api_client.issue(event.project_id, event.iid)
    end

    def team_member_to_assign
      compliance_fs(except: current_assigned_username)
    end

    def current_assigned_username
      @current_assigned_username = issue.assignees.first.username
    end

    def label_names
      @label_names ||= event.label_names
    end

    def assign_team_member_for_verification
      team_member = team_member_to_assign

      comment = <<~MARKDOWN.chomp
        This issue is ready to be verified and according to our [verification process](https://about.gitlab.com/handbook/engineering/development/sec/govern/compliance/#verification)
        we need your help with this activity.

        #{team_member}, would you mind taking a look if this issue can be verified on production and close this issue?

        /assign #{team_member}
      MARKDOWN
      add_comment(comment, append_source_link: false)
    end
  end
end
