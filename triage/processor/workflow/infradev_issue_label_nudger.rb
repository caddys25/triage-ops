# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../triage/infradev_issue_label_validator'
require_relative '../../job/infradev_issue_label_nudger_job'

module Triage
  module Workflow
    class InfradevIssueLabelNudger < Processor
      react_to 'issue.open', 'issue.update', 'issue.reopen'

      def applicable?
        event.resource_open? &&
          event.from_gitlab_org? &&
          InfradevIssueLabelValidator.new(event).applicable_event?
      end

      def process
        InfradevIssueLabelNudgerJob.perform_in(Triage::DEFAULT_ASYNC_DELAY_MINUTES, event)
      end

      def documentation
        <<~TEXT
          Posts a group label reminder to infradev issues if no ownership label is found
        TEXT
      end
    end
  end
end
