# frozen_string_literal: true

require 'digest'

require_relative 'community_processor'

require_relative '../../triage/rate_limit'
require_relative '../../../lib/discord_messenger'
require_relative '../../triage/changed_file_list'
require_relative '../../job/add_comment_job'

module Triage
  # Allows any community contributor to ask for a review.
  # The automation will pick a random MR coach and ask them to review the MR.
  class CommandMrRequestReview < CommunityProcessor
    include RateLimit

    REVIEWERS_REGEX = /@([^@[[:blank:]]]+)/
    FIVE_MINUTES = 60 * 5

    react_to 'merge_request.note'
    define_command name: 'ready', aliases: %w[review request_review], args_regex: REVIEWERS_REGEX

    def applicable?
      valid_command?
    end

    def process
      comment = <<~MARKDOWN.chomp
        /ready
        /label ~"#{Labels::WORKFLOW_READY_FOR_REVIEW_LABEL}"#{reviewer_username_action}
      MARKDOWN

      if diff_ready?
        add_comment(comment, append_source_link: false)
      else
        add_comment(no_changes_message, append_source_link: true)

        AddCommentJob.perform_in(FIVE_MINUTES, event, comment)
      end
    end

    def no_changes_message
      <<~TEXT
      @#{event.event_actor_username} this merge request currently contains no changes.
      If you have made changes, the diff may not yet be ready.

      We will wait 5 minutes before assigning a reviewer.

      If no reviewer is assigned, please use the command: `@gitlab-bot help`.
      TEXT
    end

    def documentation
      <<~TEXT
      This allows any community contributor to ask for a review and sends a message to Discord.

      The command can be used as `@gitlab-bot ready`, and is aliased to `@gitlab-bot review` and `@gitlab-bot request_review`.

      The command results in the `~"#{Labels::WORKFLOW_READY_FOR_REVIEW_LABEL}"` label being added to the merge request.

      If you know a relevant reviewer(s) (for example, someone that was involved in a related issue), you can also assign them directly with `@gitlab-bot ready @user1 @user2`.
      TEXT
    end

    private

    def reviewer_username_action
      return unless valid_reviewers.any?

      "\n/assign_reviewer #{reviewer_mentions}"
    end

    def valid_reviewers
      @valid_reviewers ||= command.args(event)
    end

    def reviewer_mentions
      valid_reviewers.map { |reviewer| "@#{reviewer}" }.join(' ')
    end

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("request_review-commands-sent-#{event.event_actor_id}-#{event.noteable_path}-#{diff_ready?}")
    end

    def rate_limit_count
      event.by_team_member? ? 100 : 1
    end

    def rate_limit_period
      3600 # 1 hour
    end

    def diff_ready?
      Triage::ChangedFileList.new(project_id, merge_request_iid).merge_request_changes.any?
    end

    def project_id
      event.project_id
    end

    def merge_request_iid
      event.iid
    end
  end
end
