# frozen_string_literal: true

require 'digest'

require_relative 'community_processor'

require_relative '../../triage/rate_limit'
require_relative '../../../lib/discord_messenger'
require_relative '../../triage/changed_file_list'
require_relative '../../job/add_comment_job'

module Triage
  # Allows any community contributor that can no longer work on an MR to
  # hand it off to the community to pick it up.
  class CommandMrHandoff < CommunityProcessor
    include RateLimit

    react_to 'merge_request.note'
    define_command name: 'handoff'

    def initialize(event)
      super(event)

      @discord_messenger = DiscordMessenger.new(ENV.fetch('DISCORD_WEBHOOK_PATH_COMMUNITY_MR_HANDOFF', nil))
    end

    def applicable?
      valid_command?
    end

    def process
      comment = <<~MARKDOWN.chomp
        We are looking for a community member to continue this MR.
        If you would like to pick it up, please leave a comment in here mentioning the Contributors Success Team with `@gitlab-org/developer-relations/contributor-success`.

        /label ~"#{Labels::WORKFLOW_IN_DEV_LABEL}" ~"#{Labels::SEEKING_COMMUNITY_CONTRIBUTIONS}"
        /unassign
      MARKDOWN

      add_comment(comment, append_source_link: true)

      post_discord_message if event.project_public?
    end

    def documentation
      <<~TEXT
        This processor supports the "@gitlab-bot handoff" triage operation command to unassign the MR
        and hand it off to the rest of the community to pick it up, also sending a notification to the Discord server.
      TEXT
    end

    private

    attr_reader :discord_messenger

    def cache_key
      @cache_key ||= OpenSSL::Digest.hexdigest('SHA256', "handoff-commands-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      event.by_team_member? ? 100 : 1
    end

    def rate_limit_period
      3600 # 1 hour
    end

    def post_discord_message
      message = <<~TEXT
        The previous assignee(s) can't work on a merge request anymore:
        - URL: #{event.url}
        - Title: `#{event.title}`
        - Project: `#{event.project_path_with_namespace}`
      TEXT

      discord_messenger.send_message(message)
    end
  end
end
