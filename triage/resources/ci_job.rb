# frozen_string_literal: true

require_relative '../../lib/devops_labels'

module Triage
  CiJob = Struct.new(:instance, :project_id, :job_id, keyword_init: true) do
    def retry
      api_client.job_retry(project_id, job_id).to_h
    end

    def trace
      @trace ||= (api_client.job_trace(project_id, job_id) || '')
    end

    def failed_rspec_test_metadata
      return [] unless rspec_job_artifact_to_json.any?

      rspec_job_artifact_to_json.select { |example| example["status"] == "failed" }.each do |example|
        labels_map = group_category_labels_for(example['feature_category'])

        if labels_map.present?
          example['product_group_label'] = labels_map['group_label']
          example['feature_category_label'] = labels_map['category_label']
        else
          example['product_group_label'] = DevopsLabels::MISSING_PRODUCT_GROUP_LABEL
          example['feature_category_label'] = DevopsLabels::MISSING_FEATURE_CATEGORY_LABEL
        end
      end
    end

    private

    def api_client
      @api_client ||= Triage.api_client(instance)
    end

    def rspec_job_artifact_file
      @failed_job_artifact ||= api_client.download_job_artifact_file(project_id, job_id, "rspec/rspec-#{job_id}.json")
    end

    def rspec_job_artifact_to_json
      @data ||= begin
        JSON.parse(rspec_job_artifact_file.read)["examples"]
      rescue StandardError
        []
      end
    end

    def group_category_labels_for(feature_category)
      DevopsLabels.group_category_labels_per_category(feature_category)
    end
  end
end
