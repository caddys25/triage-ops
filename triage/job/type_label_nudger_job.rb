# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/job'
require_relative '../triage/reaction'
require_relative '../triage/markdown_table'
require_relative '../triage/result'
require_relative '../triage/unique_comment'
require_relative '../processor/type_label_nudger'
require_relative '../../lib/constants/labels'

module Triage
  class TypeLabelNudgerJob < Job
    include Reaction

    TYPE_LABEL_MISSING_ERROR_MESSAGE = 'Please add ~"type::bug" ~"type::feature", or ~"type::maintenance" label to this merge request.'
    TYPE_IGNORE_LABEL_ERROR_MESSAGE = '~"type::ignore" is not a valid type label for merge requests and should be used only for issues. Please see [this guidance](https://about.gitlab.com/handbook/product/cross-functional-prioritization/#cross-functional-milestone-planning) for more details on ~"type::ignore".'

    private

    def execute(event)
      prepare_executing_with(event)
      return unless applicable?

      result = validate

      add_discussion(type_label_nudger_comment(result), append_source_link: false)
    end

    def applicable?
      resource_open? && !community_contribution? && need_to_nudge?
    end

    def resource_open?
      merge_request.state == 'opened'
    end

    def community_contribution?
      merge_request.labels.include?(Labels::COMMUNITY_CONTRIBUTION_LABEL)
    end

    def need_to_nudge?
      !valid_type_label? && !unique_comment.previous_discussion
    end

    def merge_request
      @merge_request ||= Triage.api_client.merge_request(event.project_id, event.iid)
    end

    def valid_type_label?
      type_label_present? && !type_ignore_label_present?
    end

    def type_label_present?
      (merge_request.labels & Labels::TYPE_LABELS).any?
    end

    def type_ignore_label_present?
      merge_request.labels.include?(Labels::TYPE_IGNORE_LABEL)
    end

    def type_label_nudger_comment(validated_result)
      table = MarkdownTable.new(validated_result).markdown

      comment = <<~MARKDOWN.chomp
        :wave: @#{event.resource_author.username} - please see the following guidance and update this merge request.
        #{table}
      MARKDOWN
      unique_comment.wrap(comment).strip
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new('Triage::TypeLabelNudger', event)
    end

    def validate
      result = Triage::Result.new

      result.errors << TYPE_LABEL_MISSING_ERROR_MESSAGE unless type_label_present?
      result.errors << TYPE_IGNORE_LABEL_ERROR_MESSAGE if type_ignore_label_present?

      result
    end
  end
end
