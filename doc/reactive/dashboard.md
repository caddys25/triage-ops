# Dashboard

[[_TOC_]]

There is a basic dashboard located at:
<https://triage-ops.gitlab.com/dashboard>

## What it shows

For now it'll show the data from `SuckerPunch::Queue.stats` as JSON, allowing
us to monitor if the service is overloading or not.

## How to log in

You can log into the dashboard with the token named as `GITLAB_DASHBOARD_TOKEN`
saved in a protected and masked variable in the
[project CI/CD settings](https://gitlab.com/gitlab-org/quality/triage-ops/-/settings/ci_cd). You can also find this token stored in the
1password Engineering value, titled "triage-ops GITLAB_DASHBOARD_TOKEN".

Use this token either as the username or the password to log in. Leave the
other blank.
