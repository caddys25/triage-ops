# frozen_string_literal: true

require_relative '../lib/community_contribution_helper'

Gitlab::Triage::Resource::Context.include CommunityContributionHelper
