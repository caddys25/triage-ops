# frozen_string_literal: true

module Labels
  COMMUNITY_CONTRIBUTION_LABEL = 'Community contribution'
  LEADING_ORGANIZATION_LABEL = 'Leading Organization'
  FIRST_CONTRIBUTION_LABEL = '1st contribution'

  UX_LABEL = 'UX'
  FRONTEND_LABEL = 'frontend'
  BACKEND_LABEL = 'backend'

  FEATURE_FLAG_LABEL = 'feature flag'

  DOCUMENTATION_LABEL = 'documentation'
  DOCS_ONLY_LABEL = 'docs-only'
  TECHNICAL_WRITING_LABEL = 'Technical Writing'
  TECHNICAL_WRITING_TRIAGED_LABEL = 'tw::triaged'

  MR_APPROVED_LABEL = 'pipeline:mr-approved'

  HACKATHON_LABEL = 'Hackathon'

  WORKFLOW_REFINEMENT_LABEL = 'workflow::refinement'
  WORKFLOW_PLANNING_BREAKDOWN_LABEL = 'workflow::planning breakdown'
  WORKFLOW_READY_FOR_DEVELOPMENT_LABEL = 'workflow::ready for development'
  WORKFLOW_IN_DEV_LABEL = 'workflow::in dev'
  WORKFLOW_READY_FOR_REVIEW_LABEL = 'workflow::ready for review'
  WORKFLOW_VERIFICATION = 'workflow::verification'
  WORKFLOW_COMPLETE = 'workflow::complete'

  WORKFLOW_STAGING_CANARY = 'workflow::staging-canary'
  WORKFLOW_STAGING = 'workflow::staging'
  WORKFLOW_STAGING_REF = 'workflow::staging-ref'
  WORKFLOW_CANARY = 'workflow::canary'
  WORKFLOW_PRODUCTION = 'workflow::production'
  WORKFLOW_ENVIRONMENTS = [
    WORKFLOW_STAGING_CANARY,
    WORKFLOW_STAGING,
    WORKFLOW_STAGING_REF,
    WORKFLOW_CANARY,
    WORKFLOW_PRODUCTION
  ].freeze

  SEEKING_COMMUNITY_CONTRIBUTIONS = 'Seeking community contributions'

  IDLE_LABEL = 'idle'
  STALE_LABEL = 'stale'

  AUTOMATION_AUTHOR_REMINDED_LABEL = 'automation:author-reminded'
  AUTOMATION_REVIEWERS_REMINDED_LABEL = 'automation:reviewers-reminded'

  FEDRAMP_VULNERABILITY_LABEL = 'FedRAMP::Vulnerability'
  VULNERABILITY_SLA_LABEL = 'Vulnerability SLA'
  VULNERABILITY_FIX_AVAILABLE_LABELS = [
    'Vulnerability::Vendor Package::Fix Available',
    'Vulnerability::Vendor Base Container::Fix Available'
  ].freeze

  INFRADEV_LABEL = 'infradev'

  TYPE_LABELS = [
    'type::feature',
    'type::maintenance',
    'type::bug'
  ].freeze

  TYPE_IGNORE_LABEL = 'type::ignore'

  SUBTYPE_LABELS = [
    'bug::performance',
    'bug::availability',
    'bug::vulnerability',
    'bug::mobile',
    'bug::functional',
    'bug::ux',
    'feature::addition',
    'feature::enhancement',
    'feature::consolidation',
    'feature::removal',
    'maintenance::refactor',
    'maintenance::dependency',
    'maintenance::usability',
    'maintenance::test-gap',
    'maintenance::pipelines',
    'maintenance::workflow',
    'maintenance::scalability'
  ].freeze

  SPECIAL_ISSUE_LABELS = [
    'support request',
    'meta',
    'triage report'
  ].freeze

  # Govern:Threat Insights labels
  THREAT_INSIGHTS_GROUP_LABEL = 'group::threat insights'
  THREAT_INSIGHTS_TEAM_LABELS = [
    'threat insights::navy',
    'threat insights::tangerine'
  ].freeze

  SPAM_LABEL = 'Spam'

  MASTER_BROKEN_LABEL = 'master:broken'
  MASTER_FOSS_BROKEN_LABEL = 'master:foss-broken'
  PIPELINE_EXPEDITE_LABEL = 'pipeline:expedite'
  FLAKY_TEST_LABEL_PREFIX = 'flaky-test::'

  QUARANTINE_LABEL = 'quarantine'

  MASTER_BROKEN_ROOT_CAUSE_LABELS = {
    default: 'master-broken::undetermined',
    flaky_test: 'master-broken::flaky-test',
    dependency_upgrade: 'master-broken::dependency-upgrade',
    failed_to_pull_image: 'master-broken::failed-to-pull-image',
    gitlab_com_overloaded: 'master-broken::gitlab-com-overloaded',
    runner_disk_full: 'master-broken::runner-disk-full',
    infrastructure: 'master-broken::infrastructure',
    job_timeout: 'master-broken::job-timeout'
  }.freeze

  # Growth team labels
  GROWTH_TEAM_LABELS = [
    'section::growth',
    'Next Up'
  ].freeze

  # Quality
  QUALITY_LABEL = 'Quality'
  ENGINEERING_PRODUCTIVITY_LABEL = 'Engineering Productivity'
  ENGINEERING_ANALYTICS_LABEL = 'Engineering Analytics'

  INFRASTRUCTURE_LABEL = 'infrastructure'

  # Database labels
  DATABASE_APPROVED_LABEL = 'database::approved'
  DATABASE_REVIEWED_LABEL = 'database::reviewed'

  # Govern:Compliance labels
  COMPLIANCE_GROUP_LABEL = 'group::compliance'
  VERIFIED_BY_AUTHOR = 'verified-by-author'

  # Groups that want their issues that are closed by a merge request to be
  # automatically tagged with a workflow::complete label
  WORKFLOW_AUTOMATION = {
    'group::gitaly' => WORKFLOW_COMPLETE,
    'group::distribution' => WORKFLOW_COMPLETE
  }.freeze

  # teams that do not have any group label
  SPECIAL_TEAM_LABELS = [
    SPECIAL_ISSUE_LABELS,
    TECHNICAL_WRITING_LABEL,
    QUALITY_LABEL,
    ENGINEERING_PRODUCTIVITY_LABEL,
    ENGINEERING_ANALYTICS_LABEL,
    INFRASTRUCTURE_LABEL
  ].freeze
end
